public class DynamicApexDemo 
{
    public List<SOBJECT> selectedObjectRecords{get;set;}
    public String selectedObject{get;set;}
    public List<String> selectedFields{get;set;}
    public ObjectPermissions  objperm {get;set;}
    Map<String,Schema.SObjectType> objectMap=new Map<String,Schema.SObjectType>();
    public List<String> selectedFieldsFromPage{get;set;}
    //Schema.DescribeSObjectResult objDetails;
    public DynamicApexDemo()
    {
        selectedFields=new List<String>();
        selectedFieldsFromPage=new List<String>();
    }
    public List<SelectOption> getObjectList()
    {
        List<SelectOption> options=new List<SelectOption>();
        
        Map<String,Schema.SObjectType> targetType = Schema.getGlobalDescribe();
        objectMap.putAll(targetType);
        options.add(new SelectOption('--None--','--None--'));
        for(String objName: targetType.keySet())
        {
            options.add(new selectOption(objName,objName));
        }
        return options;
    }
    
    public List<SelectOption> getFieldList()
    {
        List<SelectOption> options=new List<SelectOption>();
        if(selectedObject!=null)
        {
            Schema.DescribeSObjectResult typedescription = objectMap.get(selectedObject).getDescribe();
            
            Map<String, schema.Sobjectfield> resultMap =typedescription.Fields.getMap();
            
            for(String ft:resultMap.keySet())
            {
                Schema.DescribeFieldResult dfr = resultMap.get(ft).getDescribe();
                if(dfr.isAccessible())
                {
                    options.add(new SelectOption(ft,ft));
                }
            }
        }
        return options;
    }
    
    public void objectpermission()
    {
        string profilename=[select profile.name from User where id=:userinfo.getuserid()].profile.name;
    
        String query='SELECT Id, SObjectType, PermissionsRead, PermissionsCreate,PermissionsDelete, Parent.PermissionsModifyAllData,PermissionsViewAllRecords FROM ObjectPermissions WHERE parentid in (select id from permissionset where PermissionSet.Profile.name=:profilename) and SObjectType=:selectedObject';
        objperm= Database.query(query);
        
    }
    
    public void save(){
        
        update objperm;
        
    }
    
    public void FetchQuery()
    {
        //List<sObject> objectRec=new List<sObject>();
        selectedObjectRecords= new List<sObject>();
        selectedFieldsFromPage=new List<String>();
        selectedFieldsFromPage.addAll(selectedFields);
        System.debug('_______selectedFields_______'+selectedFieldsFromPage);
        if(selectedFieldsFromPage!=null)
        {
            String query='Select '+string.join(selectedFieldsFromPage,',')+' from '+selectedObject;
            for(sObject st:Database.query(query))
            {
                System.debug('____st________'+st);
                selectedObjectRecords.add(st);
            }
        }
        //return objectRec;
    }
}