public with sharing class JASearch {
public string posid;
public list<Job_Application__c> jalist{get;set;}
public string searchText{
get{
if (searchText==null) searchText='ITC';
return searchText;
}
set;
}
public JASearch(ApexPages.StandardController controller) {
}
public pagereference search (){
 /*  if (jalist == null) {
            jalist = new List<Job_Application__c>();
        } else {
            jalist .clear();
        } */
posid=apexpages.currentpage().getparameters().get('id');
jalist = [select id,Name,OMC_level__c from Job_Application__c where Position__r.Id =: posid and name like: searchText];
system.debug('++++++++jalist ++++++'+jalist);
return null;
}
}