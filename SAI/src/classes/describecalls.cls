global class describecalls {
global List<String> getPicklistValues(String ObjectApi_name,String Field_name){

    List<String> lstPickvals=new List<String>();
    Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
    system.debug('One  '+targetType );
    //Sobject Object_name = targetType.newSObject();
   // system.debug('Two  '+Object_name);
    
    //Schema.sObjectType sobject_type = Object_name.getSObjectType();
    //system.debug('Three  '+sobject_type);
    
    Schema.DescribeSObjectResult sobject_describe = targetType .getDescribe();//sobject_type
    system.debug('Four  '+sobject_describe);
    
    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
    system.debug('Map  '+field_map);
    
    List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues();
    system.debug('List  '+pick_list_values );

        for (Schema.PicklistEntry a : pick_list_values)
        {
              lstPickvals.add(a.getValue());//add the value  to our final list
        }

      String picklist = Json.serialize(lstPickvals);
      system.debug('get picklist---'+picklist);

  system.debug('Sift Status--' +lstPickvals);

   return lstPickvals;

    }
    }