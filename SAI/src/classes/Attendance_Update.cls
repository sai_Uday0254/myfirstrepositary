global class Attendance_Update {
    public static Attendance_Sheet__c sheet {get;set;}
    public static list<Attendance_Sheet__c> sheetList = new list<Attendance_Sheet__c>();
    public Attendance__c att{set;get;}
    public static integer datee;
    public boolean datecheck;
    public Id id = apexpages.currentpage().getparameters().get('id');
    public Attendance_Update(ApexPages.StandardController controller) {
    att = [select id,Address__c,Attendance_Sheet__c,Date__c from Attendance__c where id=: id];
    sheet = [select X20__c,X21__c,X22__c,X23__c from Attendance_Sheet__c where id=: att.Attendance_Sheet__c];
    system.debug('$$$'+sheet);
    datee = att.Date__c.day();
    //datecheck = false;
    //sheet.X23__c= datecheck; 
    }
    
        @RemoteAction
    // Auto checkin the user if the current user's location is in and around 0.5 kms radius of Account/Lead/Retailer address
    global static void autoCheckIn(Double lat, Double lon, Double accntLat, Double accntLng) {
       
        System.debug('User Latitude =' +lat);
        System.debug('User Longitude =' +lon);
        System.debug('Account Latitude =' +accntLat);
        System.debug('Account Longitude =' +accntLng);
        
        // convert to radians
        Double dDepartLat = lat * 3.14159 / 180;
        Double dDepartLong = lon * 3.14159 / 180;
        Double dArrivalLat = accntLat * 3.14159 / 180;
        Double dArrivalLong = accntLng * 3.14159 / 180;
        
        Double dDeltaLong = dArrivalLong - dDepartLong;
        Double dDeltaLat = dArrivalLat - dDepartLat;
        
        // calculate angle using the haversine formula
        Double dHaversineResult = Math.Sin( dDeltaLat / 2 ) * Math.Sin( dDeltaLat / 2 ) 
                                  + Math.Cos( dDepartLat ) * Math.Cos( dArrivalLat ) 
                                    * Math.Sin( dDeltaLong / 2 ) * Math.Sin( dDeltaLong / 2 );
        
        // calculate distance by multiplying arc-tangent by the planet radius in miles
        Double dDistance = 1.60934 * 3958.76 * 2 * Math.Atan2( Math.Sqrt( dHaversineResult ), Math.Sqrt( 1 - dHaversineResult ) );
        
        System.debug('Distance = ' + dDistance);
        
        GeoTagging_Distance__c gtd = GeoTagging_Distance__c.getValues('Geo Tagging');
        
        System.debug('Configured Distance = ' +gtd.Distance__c);
        //Attendance_Sheet__c sheet = new Attendance_Sheet__c ();
        
        If(dDistance <= gtd.Distance__c)
        {
                  if(datee == 23)
        sheet.X23__c = true; 
        system.debug('!!!'+sheet.X23__c);
        sheetList.add(sheet);
       
        upsert sheetList;
       
            } 
            
            }
   

}