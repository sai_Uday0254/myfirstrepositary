public class Oppsearch {
    private ApexPages.StandardController controller {get; set;}
    public Account a;
    public List<opportunity> searchResults {get;set;}
 public string searchText {
        get {
            if (searchText == null) searchText = 'Acme';
            return searchText;
        }
        set;
    }

    public Oppsearch(ApexPages.StandardController controller) {
 }
    public PageReference search() {
        if (searchResults == null) {
            searchResults = new List<opportunity>();
        } else {
            searchResults.clear();
        }
         string accid = ApexPages.currentPage().getParameters().get('id');
        String qry = 'Select o.Id, o.Name, o.StageName, o.CloseDate, o.Amount from Opportunity o Where o.accountid =: accid and  o.Name  LIKE : searchText Order By o.Name';
        searchResults = Database.query(qry);
        return null;
    }

}