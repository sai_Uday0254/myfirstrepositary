public with sharing class DemoSoqlController {
public list<account> acclist{set;get;}
public list<contact> conlist{set;get;}
public list<lead> leadlist{set;get;}

public DemoSoqlController (){}
public void demoMethod(){
acclist = new list<account>();
conlist = new list<contact>();
leadlist = new list<lead>();
list<list<sobject>> searchlist = [find 'Test' in all fields returning account(id,name), contact(name,email),lead(name,company) ];
acclist = ((list<account>)searchlist[0]);
conlist = ((list<contact>)searchlist[1]);

}

}