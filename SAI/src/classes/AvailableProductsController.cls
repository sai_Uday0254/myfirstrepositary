public class AvailableProductsController {
    public list<wrapCount> wraplist {set;get;}
    public list<OpportunityLineItem > selected {set;get;}
        public AvailableProductsController(ApexPages.StandardController controller) {
                        system.debug('NNR');

           //oppLine = new list<OpportunityLineItem > ();
           wraplist= new list<wrapCount>();
            for(OpportunityLineItem op : [select ProductCode,ListPrice,product2.Expire_date__c, product2.Name from OpportunityLineItem where product2.Expire_date__c > TODAY and OpportunityId =: apexpages.currentpage().getparameters().get('id') ]){
            wraplist.add(new wrapCount (op));
            system.debug('@@@'+wraplist);
            }
        }
        public void selectedprod(){
        selected = new list<OpportunityLineItem>();
        for(wrapCount wrapobj : wraplist){
        if(wrapobj.isSelected == True){
            selected.add(wrapobj.oppl);
        }
        }
        }
        public class wrapCount {
         public OpportunityLineItem oppl {set;get;}
        public boolean isSelected {set;get;}
        public wrapCount (OpportunityLineItem op){
        oppl = op;
        isSelected = False;
        }
        }
        public PageReference saveAsPdf (){
        
        PageReference pr = new PageReference ('/apex/AvailableProducts2?id=00628000007mO7f');
          pr.setRedirect(true);
        return null;
        }
    }