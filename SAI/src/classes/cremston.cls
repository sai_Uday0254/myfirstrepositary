public with sharing class cremston {
public integer NoOfCreamStone{set;get;}
string qid = '';
public quote qut{get;set;}
public cremston (ApexPages.StandardController controller)
{
qid = apexpages.currentpage().getparameters().get('id');
qut = [select id, TotalPrice from quote where id=: qid ];
}
public pagereference create (){
Cream_stone__c cs = new Cream_stone__c();
cs.No_of_cream_stones__c = NoOfCreamStone;
cs.Total_PriceCS__c = qut.TotalPrice ;
    insert cs;
    pagereference pr = new pagereference  ('/'+cs .id); 
    return pr;
}
}