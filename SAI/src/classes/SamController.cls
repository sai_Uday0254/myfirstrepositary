public class SamController {
public Samsung__c sam{set;get;}
public string Samvendor{set;get;}
public list<selectOption> samVendorList{set;get;}
public list<Samsung_vendor__c> vendorlist = new list<Samsung_vendor__c>();
public SamController (ApexPages.StandardController controller){
sam = new samsung__c ();
samVendorList = new list<selectOption>();
samVendorList.add(new Selectoption ('none','--None--'));
vendorlist = [select id,name from Samsung_vendor__c];
for(Samsung_vendor__c sv:vendorlist ){
samVendorList.add(new selectoption (sv.Id,sv.Name));
}
}
public pagereference save () {
sam.Vendor__c=Samvendor;
insert sam;
pagereference pf = new pagereference ('/'+sam.Id);
pf.setRedirect(true);
return pf;
}
}