@isTest(seeAllData = true)
public class Test_Term_Sheet_Controller {
   
    static User testUser;
    static position__C pos; 
    public Test_Term_Sheet_Controller() {

    }
    Static void init() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = : 'System Administrator'];
        testUser = new User(Alias = 'standt1',
        Email = 'userProfile@testuser.com',
        EmailEncodingKey = 'UTF-8',
        FirstName = 'TestLast',
        LastName = 'TestLast',
        LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_AU',
        ProfileId = p.Id,
        TimeZoneSidKey = 'Australia/Sydney',
        UserName = 'userProfile@testuser.com');
       
        insert testUser;
        pos= new position__C();
        pos.name='sai';
        pos.min_pay__c=10000;
        pos.Max_pay__c=100000;
        pos.HiringManager__c=testUser.id;
        insert pos;
        system.debug('testquotelineitem :::' +testUser );
       
    }
    private static testMethod void save() {
        init();
        Test.startTest();
        System.runAs(testUser) {

            Test.setCurrentPage(page.posdemo);
            ApexPages.StandardController sc = new ApexPages.StandardController(pos);
          ApexPages.currentPage().getParameters().put('Id', pos.Id);
            DemoController tsController = new DemoController(sc);
            tsController.getType();
             tsController.getstatus();
             tsController.getstate();
            tsController.save();


        }
        Test.stopTest();
    }
}