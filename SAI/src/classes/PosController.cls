public with sharing class PosController {

    public PosController() {

    }

public Position__c pos = new Position__c ();
public string Name{set;get;}
//public string JD{set;get;}
//public string RES{set;get;}
public Decimal Maxpay {set;get;}
public Decimal Minpay{set;get;}
public PosController (Apexpages.StandardCOntroller controller){
}
/*public void add(){
pos.Responsibilitoes__c=RES;
pos.Job_description__c=JD;
insert pos;
}*/
public void reset(){
name=null;
Maxpay  = null;
Minpay = null;
}
public pagereference submit(){
pos.Name = name;
pos.Max_pay__c =Maxpay ;
pos.Min_pay__c=Minpay;
insert pos;
pagereference p = new pagereference ('/apex/PositionVfDetail');
return p;
}
}