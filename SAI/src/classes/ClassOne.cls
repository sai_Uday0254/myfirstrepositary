public with sharing class ClassOne {
public integer OffsetSize=0;
public integer LimitSize =10;
public integer Totalrecs=0;
public boolean res{get;set;}
public list<Samsung__c> sam{get;set;}
public ClassOne (ApexPages.StandardController controller){
res = false;
Totalrecs =[select count() from Samsung__c ];
}
public void get (){
sam = [select Name,Model_Number__c,Operating_system__c from Samsung__c LIMIT :LimitSize OFFSET :OffsetSize];
res =true;
}
public void FirstPage()
{
OffsetSize = 0;
}
public void previous()
{
OffsetSize = OffsetSize - LimitSize;
}public void next()
{
OffsetSize = OffsetSize + LimitSize;
}public void LastPage()
{
OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
}
public boolean getprev()
{
if(OffsetSize == 0)
return true;
else
return false;
}
public boolean getnxt()
{
if((OffsetSize + LimitSize) > totalRecs)
return true;
else
return false;
}
}