global class createMemberInbound implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env) {
    
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
          
        String myPlainText= '';     
            
       // myPlainText = email.plainTextBody;  
        
        //List<Job_Application__c> memb = new List<Job_Application__c>();
            
        try {
            Job_Application__c mem = new Job_Application__c(Name__c= email.plainTextBody, Name = email.Subject);
            
            //memb.add(mem);
            
            insert mem;    
                        
// Save attachments, if any
for (Messaging.Inboundemail.TextAttachment tAttachment: email.textAttachments) {
  Attachment attachment = new Attachment();
 
  attachment.Name = tAttachment.fileName;
  attachment.Body = Blob.valueOf(tAttachment.body);
  attachment.ParentId = mem.Id;
  insert attachment;
}
for (Messaging.Inboundemail.BinaryAttachment bAttachment: email.binaryAttachments) {
  Attachment attachment = new Attachment();
 
  attachment.Name = bAttachment.fileName;
  attachment.Body = bAttachment.body;
  attachment.ParentId = mem.Id;
  insert attachment;
}

        } 
            
        catch (Exception e) {
            System.debug('Error is: ' + e);
        }   
          
        result.success = true;
         
        return result;
    }
}