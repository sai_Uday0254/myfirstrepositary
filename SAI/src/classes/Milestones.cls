public with sharing class Milestones {
public integer Milestones{set;get;}
String qid='';
    public Quote qut{set;get;}
    public Milestones(ApexPages.StandardController controller) {
    qid=apexpages.currentpage().getParameters().get('id');
    qut=[select totalprice,id from quote where id=:qid];
    
    }
public pagereference createtermsheet()
{
    Term_sheet__c ts= new Term_sheet__c();
        ts.Total_Price__c=qut.totalprice;
        ts.Number_of_Milestones__c=Milestones;
        ts.Quote__c=qid;
        
        insert ts;
        
        PageReference pr= new PageReference ('/'+ts.id);
        return pr;
        
    
}
}