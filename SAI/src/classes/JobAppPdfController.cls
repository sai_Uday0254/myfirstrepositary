public with sharing class JobAppPdfController {
public Job_Application__c jb{set;get;}
public JobAppPdfController (){
Id id= apexpages.currentpage().getparameters().get('id');
jb = new Job_Application__c ();
jb = [select id,name,Amount__c,Education__c,Applied_date__c from Job_Application__c  where id= : id];
}
public pagereference pdf (){
//pagereference pr = page.jobAppPdf;
pagereference  pr;
pr = new pagereference('/apex/jobAppPdf?id='+jb.Id);
Blob body = pr.getContent();
Attachment at = new Attachment();
at.name = jb.name +'Pdf';
at.ParentId = jb.id;
at.Body = body;
insert at;
pagereference prf = new pagereference ('/apex/jobAppPdf?Id='+jb.Id);
return prf;
}
}