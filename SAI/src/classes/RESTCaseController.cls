@RestResource(urlMapping='/FieldCase/*')
global with sharing class RESTCaseController {

@HttpPost   
  global static String createNewCase(String companyName, String caseType) {
     System.debug('COMPANY: '+companyName);
     System.debug('CASE TYPE: '+caseType);
     
     List<Account> company = [Select ID, Name, Email__c, BillingState from Account where Name =:companyName];
     List<Support_Member__c> support;
     
     if(company.size() > 0) {
        support = [SELECT ID, User__c, Name, User__r.Email from Support_Member__c WHERE Support_State__c =:company[0].BillingState LIMIT 1];
     }
     
     if(company.size() == 0 || support.size() == 0) {
        return 'No support data exists for this problem';
     }
     
     Case c = new Case();
     c.OwnerId = support[0].User__c;
     c.AccountId = company[0].Id;
     c.Subject = caseType + ' for '+companyName;
     c.Status = 'Open';
     insert c;
     
     //sendEmail(companyName, company[0].Email__c, support[0].Name, support[0].User__r.Email);
          
     return 'Submitted case to '+support[0].Name+' for '+companyName+'.  Confirmation sent to '+company[0].Email__c;
  }
  
  @HttpGet
  global static List<Case> getOpenCases() {
    String companyName = RestContext.request.params.get('companyName');
    Account company = [ Select ID, Name, Email__c, BillingState from Account where Name =:companyName];
     
    List<Case> cases = [SELECT Id, Subject, Status, OwnerId, Owner.Name from Case WHERE AccountId =: company.Id];
    return cases;
    
  }

}