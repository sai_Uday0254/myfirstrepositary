public with sharing class AccpdfController {
    public Account acc{get;set;}
    public PageReference attachmentmeth() {
    pagereference  pr;
    pr = new pagereference('/apex/accpdf?id='+acc.Id);
    Blob pdf =  pr.getContent() ;
    attachment a = new attachment();
    a.body = pdf;
    a.parentId = acc.id;
    a.name = acc.name+ '.pdf';
    insert a;
    PageReference pr1 = New PageReference('/apex/accpdf?id='+ acc.id);
        pr1.setRedirect(true);
        return pr1;
        
    }


    public AccpdfController() {
    string id=ApexPages.currentPage().getParameters().get('id');
    acc = new Account();
      acc=[select id,name,phone,industry,Ownership from Account where Id = : id];

    }
}