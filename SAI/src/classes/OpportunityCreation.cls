global class OpportunityCreation implements Messaging.InboundEmailHandler {

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {

     Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    
     Contact vCon;
   for(Contact c: [Select Id, Name, Email, AccountId From Contact Where Email = :email.fromAddress Limit 1])
       vCon = c;
     if(vCon == null)
     {
    vCon = new Contact(lastname = 'Your name',email = email.fromAddress);
    insert vCon;
   }   
   
     Opportunity opportunity = new Opportunity();
     opportunity.Name = email.Subject;
     opportunity.StageName = 'Prospecting';
     opportunity.CloseDate = Date.today();
     opportunity.Email_Body__c = email.plainTextBody;
     opportunity.AccountId = vCon.AccountId;
     insert opportunity;
    
       System.debug('====> Created opportunity '+opportunity.Id);
       
      
if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
      for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
        Attachment attachment = new Attachment();
        // attach to the newly created opportunity record
        attachment.ParentId = opportunity.Id;
        attachment.Name = email.binaryAttachments[i].filename;
        attachment.Body = email.binaryAttachments[i].body;
        insert attachment;
      }
    }
  
    return result;

  }

}