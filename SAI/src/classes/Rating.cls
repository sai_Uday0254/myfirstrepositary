global class Rating {
    public Rating(){}
    webservice static void approve (id QId){
    list<Quotelineitem> updatelist = new list<Quotelineitem>();
        list<Quotelineitem> quotelist = new list<Quotelineitem>();
        set<Id> prodId =new set<Id>();
        set<Id> contId =new set<Id>();
        list<string> emailList = new list<string>();
        list<contact> con = new list<contact >();
        quotelist = [select id,rating__C ,quote.AccountId ,quote.ContactId,quote.OpportunityId,product2Id from Quotelineitem where id =: QId];
        for(Quotelineitem qt: quotelist ){
        prodId.add(qt.product2Id);
        contId.add(qt.quote.ContactId);
        }
        
        product2 prod = [select id,Family from product2 where id =: prodId limit 1];
        con = [select id,email from contact where id=:contId ];
        system.debug('quotelist'+quotelist);
        for(contact co: con){
            emailList.add(co.email);
            
        }
        for(Quotelineitem qlitem: quotelist ){
        
        qlitem.rating__c = Prod_Family__c.getValues(prod.family).Rating__c;
        updatelist.add(qlitem);
        }
        update updatelist;
        
        // Step 0: Create a master list to hold the emails we'll send
  List < Messaging.SingleEmailMessage > mails =
   new List < Messaging.SingleEmailMessage > ();
  // Step 1: Create a new Email
  Messaging.SingleEmailMessage mail =
   new Messaging.SingleEmailMessage();

    mail.setToAddresses(emailList);


    mail.setSenderDisplayName('Email Alert');



    // Step 4. Set email contents - you can use variables!
    mail.setSubject('Email Alert');
    String body = 'Hi , ' + '\n';
    body += 'Please Approve  for the following Approval process' + '\n';


    mail.setHtmlBody(body);

    // Step 5. Add your email to the master list
    mails.add(mail);
    // Step 6: Send all emails in the master list
    Messaging.sendEmail(mails);
   }
  }