/**
*   Utility class used for generating Dummy Data for Test Methods
**/
public class DummyData
{
    /**
    *   Description : This method will generate List of Account with Dummy Data
    *http://www.jitendrazaa.com/blog/salesforce/apex/faq-writing-test-class-in-salesforce/
    *   Parameters :
    *   @totalRecords : How many Records you want to generate ?
    *   @withIds : Do you want returned records with generateId? If null then false
    **/
    public static List<Account> getAccounts(Integer totalRecords, Boolean withIds)
    {
        List<Account> retList = new List<Account>();
        if(withIds == null)
            withIds = false;

        for(Integer i=0;i<totalRecords;i++)
        {
            Account a = new Account(Name = constructTestString(20));
            retList.add(a);
        }
        if(withIds)
            insert retList;

        return retList;
    }

    /**
    *   This method is used to generate Random String of supplied length
    */
    public static String constructTestString(Integer length) {
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        return key.substring(0,length);
    }
}