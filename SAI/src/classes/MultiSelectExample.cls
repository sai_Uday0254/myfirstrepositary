public class MultiSelectExample {
    public List<String> cities      {set;get;}
    public Set<String> nscities     {set;get;}
    public Set<String> scities      {Set;get;}
    public List<String> selected    {Set;get;}
    public List<String> removed     {set;get;}
    public List<SelectOption> soptions{set;get;}
    public List<selectOption> nsoptions {set;get;}
    public MultiSelectExample(){
        soptions=new List<SelectOption>();
        nsoptions=new List<SelectOption>();
        cities=new List<String>{'Hyd','Ban','Che'};
        scities=new Set<String>();
        nscities=new Set<String>();
        nscities.addAll(cities);
        show();
    }
    public void show(){
        nsoptions.clear();
        soptions.clear();
        for(String s1:nscities){
            SelectOption op1=new SelectOption(s1,s1);
            nsoptions.add(op1);
        }
        for(String s2:scities){
            SelectOption op2=new SelectOption(s2,s2);
            soptions.add(op2);
        }
        
    }
    public void addEle(){
         nscities.removeAll(selected);
        scities.addAll(selected);
        show();
    }
    public void removeEle(){
        scities.removeAll(removed);
        nscities.addAll(removed);
        show();
    }

}