public with sharing class SamsungUpdateController {
public Samsung__c sam{set;get;}
    public SamsungUpdateController(ApexPages.StandardController controller) {
Id id = apexpages.currentpage().getparameters().get('id');
sam = [select id,Model_Number__c, IMEI_Number__c,Operating_system__c from Samsung__c where Id=:id ];
    }
    public pagereference save() {
    update sam;
    pagereference pr = new pagereference ('/a08/o');
    return pr;
    }

}