public class PageBlockExample {
    public String title     {Set;get;}
    public string url       {set;get;}
    public String helpText      {set;get;}
    public Boolean flag     {set;get;}
    public PageBlockExample(){
        title='Apex PageBlock';
        url='/apex/page1';
        helpText='apexHelp';
        flag=true;
    }
}