global class samvendorTest{
//public string vendid;

webservice static string getTest (id vendid){
list <samsung__c> samlist=new list <samsung__c>() ;
Set <string> samname=new Set <string>() ;
Map<String,product2> prodmap=new Map<String,product2>();

string message='';
//vendid = apexpages.currentpage().getparameters().get('id');
samlist = [select id,name,Device_type__c,IMEI_Number__c,Model_Number__c,Operating_system__c,Price__c from samsung__c where Vendor__c =: vendid ];

for(samsung__c sm:samlist ){
samname.add(sm.name);
}
product2 prod1 = new product2();
list<product2 > prodlistNew = new list<product2 >();
list<product2 > prodlist = new list<product2 >([select id,name from product2  where name in :samname]);
for(product2 pr: prodlist){
prodmap.put(pr.name,pr);
}
for(samsung__c sm:samlist ){
if(prodmap.get(sm.name)!=null)
{
message = message+ sm.name+' record already prod '+'\n';
}
else{
prod1.name = sm.name;
prodlistNew.add(prod1);
message = message+ sm.name+' record has succefully inserted '+'\n';

}

}
insert prodlistNew;
return message;
}
}