@isTest
Public class Test_JobAppController {
public static Job_Application__c jobApp;
public static Candidate__c cand;
public static void init(){
jobApp = new Job_Application__c (Cover_letter__c='Test Letter',Date_check__c=True,Applied_date__c=system.today(),Amount__c=100);
insert jobApp;
system.debug('###'+jobApp);
cand = new Candidate__c (Job_Application__c=jobApp.Id,First_Name__c='First',Last_Name__c='Last');
insert cand;
system.debug('###'+cand);
}
public static testmethod void Jac (){
init();
ApexPages.StandardController Sc = new ApexPages.StandardController (jobApp);
//ApexPages.CurrentPage().getParameters().put('Id','');
JobAppController jc = new JobAppController(sc);
jc.AddMore();
jc.save();
} 
}