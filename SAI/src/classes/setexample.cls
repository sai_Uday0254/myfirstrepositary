public with sharing class setexample {
public set <string> names{set;get;}
public boolean test {set;get;}
public setexample (){
list <string> myval = new list <string> ();
myval.add('sai');
myval.add('sam');
myval.add('ram');
names = new set<string>();
names.add('one');
names.add('two');
names.add('three');
test = names.retainAll(myval);
//names.add('myval');

}

}