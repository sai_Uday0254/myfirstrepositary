public with sharing class web2lead {
public lead weblead;
public web2lead (apexpages.standardController stdcontroller){
weblead = (Lead)stdcontroller.getrecord();
}
public pagereference saveLead (){
try{
insert (weblead);
}
catch(System.DMLException e) {
    ApexPages.addMessages(e);

}
return null;
}
}