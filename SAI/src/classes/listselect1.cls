public with sharing class listselect1 {
public string myval {set;get;}
public list <selectoption> myoptions;
public list <selectoption> getmyoptions()
{
return myoptions;
}
public listselect1 (){
myoptions = new list<selectoption>();
selectoption s1 = new selectoption('null','-None-');
selectoption s2 = new selectoption ('one', 'Jan');
selectoption s3 = new selectoption ('two', 'Feb');
selectoption s4 = new selectoption ('three', 'Mar');
selectoption s5 = new selectoption ('four', 'Apr');
selectoption s6 = new selectoption ('five', 'May');
selectoption s7 = new selectoption ('six', 'Jun');
myoptions.add (s1);
myoptions.add (s2);
myoptions.add (s3);
myoptions.add (s4);
myoptions.add (s5);
myoptions.add (s6);
myoptions.add (s7);
myoptions.add (new selectoption ('seven','July'));
}


}