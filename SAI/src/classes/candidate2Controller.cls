public with sharing class candidate2Controller {
public Candidate__c cand{set;get;}
public candidate2Controller (Apexpages.StandardController Controller){
cand = new Candidate__c ();
Id id = apexpages.currentpage().getparameters().get('id');
cand = [select id,First_Name__c,Last_Name__c,SSN__c from Candidate__c where Id=:id];

}
public pagereference save(){
update cand;

pagereference pr = new pagereference ('/apex/candidateview?id='+cand.id);
system.debug('------pr------'+pr);
return pr;
}
}