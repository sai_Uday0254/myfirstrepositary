public class OppPdfController {
public opportunity opp {set;get;}
public Id id = apexpages.currentpage().getparameters().get('id');
public OppPdfController (){
opp = [select id,name,StageName,CloseDate,Probability,OwnerId,Status__c from Opportunity where id= : id];
}
public void SavePdf (){
system.debug('emails::');
pagereference pref = new pagereference ('/apex/OppPdf_Temp?id='+id);
blob bob = pref.getContentAsPdf();
Attachment att = new Attachment ();
att.name = opp.name +'Pdf';
att.ParentId = opp.id;
att.body = bob;
insert att;
User usr = [select id,name,ManagerId from user where id = : userinfo.getuserid()];
system.debug('usr::'+usr);
user managerEmail = [select id,email from user where id = : usr.ManagerId  ];
list <string> emails = new list <string>();
emails.add(managerEmail.email);
system.debug('emails::'+emails);
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(emails);
//mail.setCcAddresses('spullamsetty@bodhtree.com');
mail.setSubject('Opp Attachment');
mail.setPlainTextBody('This is test email body. This mail is being sent from apex code');
list<Messaging.Emailfileattachment> AttachList = new list<Messaging.Emailfileattachment>();
for(Attachment a : [select name,body,ParentId from Attachment where ParentId=: id ]){
Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
efa.setFileName(att.Name);
efa.setBody(att.Body);
AttachList.add(efa);
}
//mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
mail.setFileAttachments(AttachList);
//mail.setHtmlBody('<b> This is HTML body </b>' );
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}