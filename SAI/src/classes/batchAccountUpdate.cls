global class batchAccountUpdate implements Database.Batchable<sObject>  {
global database.querylocator start (database.batchableContext bc)
{
string s = 'select id,name from account';
return database.getquerylocator(s);
}
global void execute(database.batchableContext bc, list<account> acc)
{
list<account> acclist = new list<account>();
for(account ac: acc){
ac.Name = ac.name+'update';
acclist.add(ac);
}
update acclist;
}
global void finish (database.batchableContext bc)
{
}
}