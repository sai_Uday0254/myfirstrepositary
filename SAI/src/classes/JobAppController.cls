public with sharing class JobAppController {
    public Job_Application__c jb {set;get;}
    public Job_Application__c jba {set;get;}
    //public list<Job_Application__c> joblist {set;get;}
    public list<Candidate__c> candlistView {set;get;}
    public list<Candidate__c> cand {set;get;}
    public list<Candidate__c> candlist = new list<Candidate__c>();
    public Id id;
    public JobAppController(ApexPages.StandardController controller) {
            system.debug('####In controller');
    jb = new Job_Application__c ();
        cand = new list<Candidate__c>();
    id = apexpages.currentpage().getparameters().get('id');
    system.debug('####'+id);
    if(id!=null)
    jba =  [select id,name,Applied_date__c,Cover_letter__c,Name__c,Date_check__c,Status__c from Job_Application__c where id= : id];
    candlistView = [select id,name,First_Name__c,Last_Name__c,Job_Application__c from Candidate__c where Job_Application__c = : id];
    system.debug('####'+candlistView);
    Candidate__c can = new Candidate__c ();
    cand.add(can);
    
     }
     public pagereference save (){
     insert jb;
     for(Candidate__c cn: cand){
        cn.Job_Application__c = jb.id;
         candlist.add(cn);
     }
     insert candlist;
      pagereference pf = new pagereference('/apex/jobAppVfView?Id='+jb.id);
      pf.setRedirect(true);
      return pf;
     }
     public void AddMore (){
        Candidate__c can1 = new Candidate__c ();
      cand.add(can1);
     }
     /*public pagereference Edit(){
     pagereference pf = new pagereference('/apex/JobAppVfEdit?Id='+jb.id);
     pf.setRedirect(true);
      return pf;
     } */

}