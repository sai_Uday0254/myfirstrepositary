public  class CustomSettingController2 {
public position__c pos {get;set;}

public CustomSettingController2 () {
pos = new position__c();
}
public pagereference save (){
Pos_setting__c PosCus=Pos_setting__c.getvalues('PosFields');
pos.Open_date__c = PosCus.Date_One__c;
pos.Closed_date__c = PosCus.Date_Two__c;
insert pos;
pagereference pr =new pagereference ('/'+pos.Id);
return pr;
}
}