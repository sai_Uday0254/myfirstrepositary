public  class DemoController {

    

public  string PositionName{set;get;}
public  string location{set;get;}
public  string JobLevel{set;get;}
public  string FunctionalArea{set;get;}
public decimal MaxPay {set;get;}
public decimal MinPay {set;get;}
public string shift{set;get;}
public string status1{set;get;}
public string country{set;get;}
public string state1{set;get;}

public DemoController (ApexPages.StandardController Controller){
}
public list<SelectOption>getType(){
list<selectOption> Type = new list<selectOption>();
type.add(new SelectOption ('--','None'));
type.add(new SelectOption ('day','FT'));
type.add(new SelectOption ('Part Time','PT'));
return Type;
}
public list<SelectOption> getstatus(){
list<Selectoption> status = new list <SelectOption>();
status.add(new selectoption('--None--','--None--'));
status.add(new selectoption('New position','New position'));
status.add(new selectoption('Pending Approval','Pending Approval'));
return status ;
}
public list<selectoption> getstate(){
list<selectoption> state = new list<selectoption>();
if (country=='India'){
state.add(new selectoption('--None--','--None--')); 
state.add(new selectoption('AP','AP'));
state.add(new selectoption('UP','UP'));
state.add(new selectoption('MP','MP'));
}
else if (country=='USA'){
state.add(new selectoption('--None--','--None--'));
state.add(new selectoption('NJS','NJS'));
state.add(new selectoption('NYK','NYK'));
state.add(new selectoption('LA','LA')); 
}
else if (country == 'USSR'){
state.add(new selectoption('--None--','--None--'));
state.add(new selectoption('MSCOW','MSCOW')); 
}
return state;
}
public pagereference save (){
system.debug('HIIIII');
Position__c pos = new Position__c();
pos.Name = PositionName;
pos.location__c = location;
pos.Job_Level__c = JobLevel;
pos.Functional_Area__c = FunctionalArea;
pos.Min_pay__c=MinPay ;
pos.Max_pay__c= MaxPay;
pos.Type__c = shift;

insert pos;
pagereference pr = new pagereference ('/'+pos.id);
return pr ;
}
}