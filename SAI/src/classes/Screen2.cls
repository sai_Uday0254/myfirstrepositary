public with sharing class Screen2 {
public Account account{set;get;}
public Screen2 (){
Id id = apexpages.currentpage().getparameters().get('id');
account = [select Id,name,phone from Account where Id =: id];    
}
public pagereference save (){
update account;
pagereference pr = new pagereference ('/apex/screen1?id='+account.id);
return pr;
}
}