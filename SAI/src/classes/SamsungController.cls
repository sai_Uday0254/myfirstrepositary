public with sharing class SamsungController {
public Samsung__c sam{set;get;}
public string country1{set;get;}
public SamsungController (Apexpages.standardController controller) {
sam = new Samsung__c ();
 Id id= apexpages.currentpage().getparameters().get('id');
}
public list<selectoption> getcountry(){
list <selectoption> country = new list<selectoption>();
country.add(new selectoption('--None--','--None--'));
country.add(new selectoption('Ind','Ind'));
country.add(new selectoption('JPN','JPN'));
return country;
}
public pagereference save() {
insert sam;
pagereference pr = new pagereference ('/apex/samsungUpdate?id='+sam.id);
return pr;
}
}