public class Prod {
Public list<Product2 > prodlist{get;set;}
public string accid;
    public Prod(ApexPages.StandardController controller) {
    accid=apexpages.currentpage().getparameters().get('id');
    //prodlist = [select id,name,Mfg_Date__c,IsActive from Product2 where IsActive=true];
    prodlist = [select id,name,Mfg_Date__c,IsActive from Product2 where IsActive=true and Account_Name__c =: accid];
    if(prodlist.isempty())
    prodlist = [select id,name,Mfg_Date__c,IsActive from Product2 where Account_Name__c =: accid];
    }

}