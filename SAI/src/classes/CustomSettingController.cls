public class CustomSettingController {
public OppertunitySettings__c customopp;
public CustomSettingController ()
{
 opp=new Opportunity ();
}
public OpportunityCustom__c cust{set;get;}
public Opportunity  opp{get;set;}
public pagereference save(){
insert opp;
customopp = OppertunitySettings__c.getValues('Test');
opp.TrackingNumber__c = string.valueof((customopp.TrackingNumber__c));
opp.Quantity__c= customopp.Quantity__c;
update opp;
PageReference pg = new PageReference('/'+opp.id);
return pg;
}
}