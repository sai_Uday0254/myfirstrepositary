public with sharing class ProjectProgressBarExtensionController {

    public Id projectId {get;set;}
    public Project__c project {get;set;}
    public List<Status> statuses {get;set;}
    public Integer countStatuses {get;set;}
    public String tabWidth {get;set;}

//-------------------------------------------------------------------------------------------------------
//  Constructor
//-------------------------------------------------------------------------------------------------------

    public ProjectProgressBarExtensionController(ApexPages.StandardController stdController) {
        try {
            this.projectId = ((Project__c)stdController.getRecord()).Id;
            refresh();
        } catch(Exception ex) {
                System.debug(ex.getStackTraceString());
        }    
    } 

    public void refresh() {
        this.project = [select Status__c from Project__c where Id =:this.projectId];
        List<String> statusNames = ProjectManager.getStatusValues();
        this.statuses = new List<Status>();
        for(String s : statusNames) {
            Status sts = new Status(this.project, s, ProjectManager.getProjectStatusLabel(s), this);
            if (this.project.Status__c == s) sts.isCurrent = true;
            Project__c toStatusOrd = new Project__c();
            toStatusOrd.Status__c = s;
            if (ProjectManager.checkIfAllowedStatusChange(this.project, toStatusOrd)) sts.changePossible = true;

            this.statuses.add(sts); 
        } 

        this.countStatuses = statuses.size();
        this.tabWidth = 'width: ' + 100/(Decimal)countStatuses + '%;';
    }

//-------------------------------------------------------------------------------------------------------
//  Status wrapper class
//-------------------------------------------------------------------------------------------------------

    public class Status {
        public Project__c project {get;set;}
        public String name {get;set;}
        public String label {get;set;}
        public Boolean isCurrent {get;set;}
        public Boolean changePossible {get;set;}
        public ProjectProgressBarExtensionController ctrl {get;set;}

        public Status(Project__c project, String name, String status, ProjectProgressBarExtensionController ctrl) {
            this.project = project;
            this.name = name;
            this.label = status;
            this.ctrl = ctrl;
        }

        public void goToStatus() {
            this.project.Status__c = this.name;
            try {
                update this.project;
            } catch (Exception ex) {

            }

            this.ctrl.refresh();
        }

    }
}