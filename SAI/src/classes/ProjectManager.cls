public class ProjectManager
{
//-------------------------------------------------------------------------------------------------------------------------------------
// Project statuses
//-------------------------------------------------------------------------------------------------------------------------------------
    public static final String STATUS_SHOPPING_CART = 'Shopping Cart';
    public static final String STATUS_ADDRESS_BOOK = 'Address Book';
    public static final String STATUS_SHIPPING_OPTIONS = 'Shipping Options';
    public static final String STATUS_PAYMENT_INFO = 'Payment Info';
    public static final String STATUS_ORDER_CONFIRMATION = 'Order Confirmation';
    public static final String STATUS_ORDER_COMPLETED = 'Order Completed';

    public static final Schema.DescribeFieldResult STATUS_PICKLIST = Project__c.Status__c.getDescribe();
    public static final List<Schema.PicklistEntry> STATUS_PICKLIST_VAL = STATUS_PICKLIST.getPicklistValues();

    public static String getProjectStatusLabel(String status) {
        List<Schema.PicklistEntry> ple = STATUS_PICKLIST_VAL;
        String result = '';
        for(Schema.PicklistEntry p : ple) {
            if (p.getValue() == status) {
                result = p.getLabel();
                break;
            }
        }
        return result;
    }
    
    public static List<String> getStatusValues(){
        List<Schema.PicklistEntry> ple = STATUS_PICKLIST_VAL;
        List<String> values = new List<String>();
        for(Schema.PicklistEntry p : ple) {
            values.add(p.getValue());
        }
        
        return values;
    }

    public static Map<String, Set<String>> ALLOWED_STATUS_CHANGES()
    {
        Map<String, Set<String>> statusMap = new Map<String, Set<String>>();
      
        statusMap.put(STATUS_SHOPPING_CART, new Set<String>{STATUS_ADDRESS_BOOK});
        statusMap.put(STATUS_ADDRESS_BOOK, new Set<String>{STATUS_SHIPPING_OPTIONS});
        statusMap.put(STATUS_SHIPPING_OPTIONS, new Set<String>{STATUS_PAYMENT_INFO});
        statusMap.put(STATUS_PAYMENT_INFO, new Set<String>{STATUS_ORDER_CONFIRMATION});
        statusMap.put(STATUS_ORDER_CONFIRMATION, new Set<String>{STATUS_ORDER_COMPLETED});
        statusMap.put(STATUS_ORDER_COMPLETED, new Set<String>{});

        return statusMap;
    }

    public static Boolean checkIfAllowedStatusChange(Project__c fromStatus, Project__c toStatus) {
        Boolean allowed = false;
        Map<String, Set<String>> statusMap = ALLOWED_STATUS_CHANGES();
        if (statusMap.get(fromStatus.Status__c).contains(toStatus.Status__c)) {
            allowed = true;
        }
        return allowed;
    }

}