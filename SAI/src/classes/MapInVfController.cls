public with sharing class MapInVfController {
public map <string,list<string>> Colormap{get;set;}
public MapInVfController (){
Colormap = new map<string,list<string>>();
Colormap.put('Model A',new list<string>{'1','2','3','4'});
Colormap.put('Model B',new list<string>{'5','6','7','8'});
system.debug('Colormap:::'+Colormap);
}
}