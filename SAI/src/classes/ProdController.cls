public with sharing class ProdController {
public string oefid ;
public boolean check{set;get;}
public list<Product2> prodlist{set;get;}

    public ProdController(ApexPages.StandardController controller) {
 oefid = apexpages.currentpage().getparameters().get('id');
 check=false;
 getProd();
    }
    public void getProd(){
   check=true;
   DateTime myDatetime = DateTime.now();
String myDatetimeStr = myDatetime.format('yyyy-MM-dd');
Date myDate = date.valueOf(myDatetimeStr);

    system.debug('########oefid####### '+oefid );
    prodlist = [select id,name,ProductCode from Product2 where Products__r.id=: oefid and Mfg_Date__c > :myDate];
    //prodlist = [select id,name,ProductCode from Product2 where Products__r.id=: oefid];
    }

}