trigger candidatecount on Candidate__c (after insert,after delete) {
set<Id> canId = new set<id>();
set<Id> JobId= new set<id>();
map<Id,Integer> mapcount = new map<Id,Integer>();
list<Candidate__c> canlist = new list<Candidate__c>();
list<Candidate__c> canlistcount = new list<Candidate__c>();
list<Job_Application__c> jblist = new list<Job_Application__c>();
list<Job_Application__c> jblistcount = new list<Job_Application__c>();
    if(trigger.isInsert){
  for(Candidate__c c: trigger.new){
  JobId.add(c.Job_Application__c);
  }
  }
  if(trigger.IsDelete){
  for(Candidate__c ca: trigger.old){
  JobId.add(ca.Job_Application__c);
  }
  }
  canlist =[select id,name,Job_Application__c from Candidate__c  where Job_Application__c =: JobId];
  jblist =[select id,name from Job_Application__c where Id=: JobId];
  for(Job_Application__c jc :jblist ){
  for(Candidate__c  c: canlist ){
  c.Job_Application__c = jc.id;
  canlistcount.add(c);
  mapcount.put(jc.Id,canlistcount.size());
  }
  }
  for(Job_Application__c jb:jblist){
  jb.Amount__c =mapcount.get(jb.id);
  jblistcount.add(jb);
  }
  if(jblistcount!=null){
  update jblistcount;
  }

}