trigger UpdateTask on Contact (after Update) {
set<Id> conid = new set<Id>();
list<task> updatetasklist = new list<task>();
for(contact con: trigger.new){
conid.add(con.Id);
}
map<id,contact> conmap= new map<id,contact>([select id,name,mobilephone,(select id,contact_mobile_number__C from tasks) from contact where id in:conid]);
//map<Id,task>> tmap = new map<Id ,list<task>>([select id,whoId,Contact_Mobile_Number__c from task where whoId in:conid]);
for(contact con: conmap.values()){

list<task> tasklist = con.tasks;
for(task tas: tasklist){
tas.contact_mobile_number__C = con.mobilephone;
updatetasklist.add(tas);
}


}
update updatetasklist;
}