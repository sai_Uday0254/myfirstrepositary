trigger Bookmap on BookName__c (after insert, after update) {
list<BookName__c > booklist = new list<BookName__c >();
Map<String, BookName__c > myMap = new Map<String, BookName__c >(); 
for(BookName__c  objCS : [Select z.Name, z.Id From BookName__c z]){
        myMap.put(objCS.Name, objCS);
        booklist .add(objCS );
        }
        update booklist;
}