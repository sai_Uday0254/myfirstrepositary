trigger qline on QuoteLineItem (after insert,after update) {
set<Id> qlineid = new set<Id>();
Map<Id,User> usrmap = new Map<Id,User>([select id,Trigger_Test__c From User where Id=: UserInfo.getUserId()]);
list<QuoteLineItem> qlist = new list<QuoteLineItem>();
list<QuoteLineItem> qlist2 = new list<QuoteLineItem>();

for(QuoteLineItem ql: trigger.new){
qlineid.add(ql.Id);
}
qlist = [select id,Trigger_Test__c,createdbyId from QuoteLineItem where Id In : qlineid  ];
for(QuoteLineItem ql2 : qlist){
system.debug('----'+ql2.Trigger_Test__c);
if(ql2 .Trigger_Test__c==null){
User userobj =  usrmap.get(ql2.createdbyId);
system.debug('--userobj -'+userobj);
ql2 .Trigger_Test__c = userobj.Trigger_Test__c;
qlist2.add(ql2);
system.debug('--qlist2--'+qlist2);
}
}
if(!qlist2.isEmpty())
update qlist2;
}