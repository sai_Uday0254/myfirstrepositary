trigger DeleteChildJobapps on Position__c (before delete, after delete,after insert,after update,before update) {
set<Id> posId= new set<Id>();
if(Trigger.isafter && trigger.IsDelete){
system.debug('@@@'+trigger.new);
system.debug('@@@'+trigger.old);
for(Position__c jb: trigger.old){
if(jb.Status__c =='Pending Approval')
jb.addError('Change The Status');
else
{
posId.add(jb.Id);
system.debug('@@@'+posId);
list<Job_Application__c> joblist = [select id from Job_Application__c where Position__c=: posId];
delete joblist;
}
}
}
if(trigger.isafter && (trigger.isInsert || trigger.isUpdate)){
list<Job_Application__c> ja = [select id,Position__c from Job_Application__c where Position__c =: trigger.newmap.keyset()];
map<id,list<Job_Application__c>> jobMap = new map<id,list<Job_Application__c>>();
for(Job_Application__c jb: ja){
if(jobMap.containsKey(jb.Position__c)){
list<Job_Application__c> jba = jobMap.get(jb.position__c);
jba.add(jb);
}
else
jobMap.put(jb.Position__c,new list<Job_Application__c>{jb});
}
if(Trigger.Isinsert && trigger.Isafter){
system.debug('@@@@1');
list<Job_Application__c> jblist = new list<Job_Application__c>();
for(Position__c  pos: trigger.new){
Job_Application__c jb = new Job_Application__c();
jb.Position__c = pos.Id;
jb.name = pos.name+'From Trigger';
jblist.add(jb);
}
if(jblist.size() >0)
insert jblist;
}
if(trigger.isAfter && trigger.isUpdate){
    system.debug('@@@@2');
    list<Job_Application__c> jblist;
    list<Job_Application__c> jblist2 = new list<Job_Application__c>();
for(Position__c  pos: trigger.new){
if(pos.Functional_Area__c=='Miscellaneous'){
jblist = jobMap.get(pos.Id);
if(jblist.size()>0){
    for(Job_Application__c jb: jblist)
    {
        jb.name = pos.name;
        jb.position__c = pos.Id;
        jblist2.add(jb);
    }
}
//pos.Travel_required__c = false;
}
}
if(jblist2.size()>0)
    update jblist2;
}
}
if(trigger.isbefore && trigger.isUpdate){
for(Position__c  pos: trigger.new){
	system.debug('@@@@'+pos);
pos.Status__c ='Closed - Canceled';
}
}
}