trigger ContactMobileNumUpdate on Task (before insert) {
//when ever a task is created contact mobile num is updated in task
set<Id> ConId = new set<Id>();
for(Task t: Trigger.new){
ConId.add(t.WhoId);
}
map<id,contact> conmap = new map<id,contact>([select id,name,MobilePhone from contact where id in : ConId ]);
for(task ta: trigger.new){
contact con  = conmap.get(ta.WhoId);
ta.Contact_Mobile_Number__c = con.MobilePhone;
}

}