trigger AmountUpdateOnPosition on Job_Application__c (after update, after insert) {
set<Id> PosId = new set<Id> ();
List<Position__c> pList = new List<Position__c>();

Map<String,AggregateResult> results = new Map<String,AggregateResult>();
for(Job_Application__c Ja: trigger.new){
PosId.add(Ja.Position__c);
}

List<AggregateResult> agrresult=[select Position__c pos , max(Amount__c)Amt from Job_Application__c group by Position__c having Position__c in : PosId];
for(AggregateResult ag : agrresult)
{
    results.put(string.valueof(ag.get('pos')),ag);
}
List<Position__c> pos = [select id, amount__c from Position__c where id in :PosId];
for(Position__c pos1: pos ){
pos1.amount__c = integer.valueof(results.get(pos1.id).get('Amt'));
pList.add(pos1);
}
update pList;
}