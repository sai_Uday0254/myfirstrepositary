trigger AmountTarget on Opportunity (after insert,after update) {
//decimal sum;
target__c target1 = [select id,Start_date__c,End_Date__c,Achieved__c from target__c where User__c=: userinfo.getuserid() limit 1 ];
List<aggregateResult> results = [select CALENDAR_YEAR(CreatedDate), sum(amount) optyamt from Opportunity where CreatedDate >: target1.Start_date__c and CreatedDate <:  target1.End_Date__c and OwnerId=: userinfo.getuserid() GROUP BY CALENDAR_YEAR(CreatedDate)];
system.debug('###'+results);
/*for(aggregateResult ag:results){
//decimal sum = (Decimal)ag.get('optyamt');
sum = (Decimal)ag.get('optyamt');
system.debug('@@@'+sum);
} */
object sum = results[0].get('optyamt');
target__c target = [select id,Achieved__c from target__c where OwnerId=: userinfo.getuserid()];
target.Achieved__c = string.valueof(sum) ;
update target ;
system.debug('==='+target.Achieved__c);
}