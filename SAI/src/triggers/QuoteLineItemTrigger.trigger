trigger QuoteLineItemTrigger on QuoteLineItem (after insert){


    map<string,string> mapQuoteOppty=new map<string,string>();
    string JSONContent=Json.Serialize(Trigger.New);
    JSONParser parser =JSON.createParser(JSONContent);
    list<string> OpptyLineId=new list<string>();
    list<string> QuoteLineId=new list<string>();
    System.debug('parser-------->'+parser );
    
    while (parser.nextToken() != null) 
    {
        if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='OpportunityLineItemId')
        OpptyLineId.add(parser.getText());
        system.debug('OpptyLineId::'+OpptyLineId);
            if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='Id')
                QuoteLineId.add(parser.getText());
            system.debug('QuoteLineId::'+QuoteLineId);
                parser.nextToken();
                    if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='OpportunityLineItemId')
                        OpptyLineId.add(parser.getText());
                        if(parser.getCurrentToken()==JSONToken.VALUE_STRING && parser.getCurrentName()=='Id')
                            QuoteLineId.add(parser.getText());
    }

    System.debug('OpptyLineId-------->'+OpptyLineId);
    System.debug('QuoteLineId-------->'+QuoteLineId);
    
    integer iCount=0;
    for(string strOppLineId : OpptyLineId)
    {
        string iQuoteLineId=QuoteLineId[iCount];
        system.debug('iQuoteLineId::'+iQuoteLineId);
        mapQuoteOppty.put(iQuoteLineId,strOppLineId);
        iCount++;
    }
    Set<Id> SetOppId=new Set<Id>();
    Set<Id> SetQuoteId=new Set<Id>();
    for(QuoteLineItem QLI:trigger.new)
    {
        SetQuoteId.add(QLI.QuoteId);
    }
    List<Quote> Lstquotes =[select id, OpportunityId, isSyncing from Quote where Id in :SetQuoteId];
    for(Quote Qt:Lstquotes)
    {
        SetOppId.add(Qt.OpportunityId);
    }
    
    List<OpportunityLineItem> lstOLI=[select Id, OpportunityId,Include__c from OpportunityLineItem where OpportunityId in:SetOppId];
    
    Map<Id,OpportunityLineItem> MapOLI=new Map<Id,OpportunityLineItem>([select Id,Include__c,OpportunityId from OpportunityLineItem where OpportunityId in:SetOppId]);
    Map<Id,QuoteLineItem > MapQLI=new map<Id,QuoteLineItem>([Select Id, Include__c from QuoteLineItem where QuoteId in:SetQuoteId]);
    
    list<QuoteLineItem> updateQuoteLineItem =new list<QuoteLineItem >();
    for(QuoteLineItem qli:MapQLI.values())
    {
       System.debug('&&&&'+mapQuoteOppty);
       if(mapQuoteOppty.get(qli.id)!=null)
       {
          String OppID = mapQuoteOppty.get(qli.id);
          OpportunityLineItem OLI = MapOLI.get(OppID);
          if(OLI.Include__c==True)
          {
          qli.Include__c=OLI.Include__c;
          updateQuoteLineItem.add(qli);
          }
       }
    }
    system.debug('++++'+updateQuoteLineItem.size());
    update updateQuoteLineItem;
    list<QuoteLineItem> deletelist = [Select Id, Include__c from QuoteLineItem where QuoteId in:SetQuoteId And Include__c = false];
    system.debug('++++'+deletelist.size());
    delete deletelist;
}