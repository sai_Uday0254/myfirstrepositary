trigger ReviewOnJA on Review__c (after insert,after update,after delete) {
list <id> jaid = new list<id>();
list<Job_Application__c> jba = new list <Job_Application__c>();
list<Job_Application__c> jba2 = new list <Job_Application__c>();
if(trigger.isinsert){
for (Review__c rev : trigger.new){
jaid.add(rev.Job_Application__c);
}
}
else if(trigger.isdelete){
for (Review__c rev : trigger.old){
jaid.add(rev.Job_Application__c);
}
}
jba  = [select id,Name__c,(select id,First_Name__c,Last_Name__c from Reviews1__r) from Job_Application__c where id in : jaid];

if(trigger.isinsert){
for(Job_Application__c job : jba ){
for(Review__c rw : trigger.new){
if(rw.Job_Application__c ==job.id){
if (job.Name__c==null){
job.Name__c = rw.First_Name__c+' '+rw.Last_Name__c;
}
else
{
job.Name__c =job.Name__c+','+rw.First_Name__c+' '+rw.Last_Name__c;
}
}
}
jba2.add(job); 
}
}
if(trigger.isdelete){

for(Job_Application__c job : jba ){
job.Name__c =null;
for(Review__c rw : job.Reviews1__r ){

if (job.Name__c==null){
job.Name__c = rw.First_Name__c+' '+rw.Last_Name__c;
}
else
{
job.Name__c =job.Name__c+','+rw.First_Name__c+' '+rw.Last_Name__c;
}
}
jba2.add(job); 
}
}
update jba2;
}