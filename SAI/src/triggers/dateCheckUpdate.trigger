trigger dateCheckUpdate on Job_Application__c (before insert, before update) {
set<id> pos = new set<id>();
list<Position__c> post= new list<Position__c>();
for (Job_Application__c  ja : trigger.new){
pos.add(ja.Position__c);
}
post = [select id,Apex__c from Position__c where id in: pos];
if(trigger.isinsert){
for(Job_Application__c job : trigger.new){
for(Position__c po:post){
if(po.Apex__c==false){
job.Date_check__c=false;
}
else {
job.Date_check__c=true;
}
}
}
}
if(trigger.isupdate){
for(Job_Application__c job : trigger.new){
for(Position__c po:post){
if(po.Apex__c==false){
job.Date_check__c=false;
}
else {
job.Date_check__c=true;
}
}
}
}
}