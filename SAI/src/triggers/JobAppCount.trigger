trigger JobAppCount on Job_Application__c (after insert) {
list<id> posid = new list<id>();
list<Position__c> poslist = new list<Position__c>();
map<id,Position__c> posmap;
if(trigger.isinsert){
for (Job_Application__c job: trigger.new ){
posid.add(job.Position__c); 
}
posmap = new map<id,Position__c>([select id, Number_of_Job_Applications__c,(select id from Job_Application__r)from Position__c where id in: posid]);
for(Job_Application__c jo: trigger.new){
Position__c post = posmap.get(jo.Position__c);
post.Number_of_Job_Applications__c = post.Job_Application__r.size();
poslist.add(post);
}
update poslist;
}
}