trigger ContactNamesonAccount on Contact (after insert,after update,after delete,after undelete)
{
    Set<Id> actids=new Set<Id>();
    List<account> aacL= new List<account>();
    List<account> accList= new List<account>();
    if(Trigger.isInsert||Trigger.isUpdate||Trigger.isUndelete)
    {
        for(Contact cont:Trigger.New)
        {
            actids.add(cont.AccountId);
        }
    }
    
    if(Trigger.isDelete)
    {
        for(Contact cont:Trigger.old)
        {
            actids.add(cont.AccountId);
        }
    }
    accList=[select id,Contact_Names__c,(select id, lastname from contacts) from Account where id in :actids];
    System.debug('accList::'+accList);
    if(Trigger.isInsert||Trigger.isUndelete)
    {
        for(Account acc:accList)
        {
            for(Contact c:Trigger.new)
            {
                system.debug('contact:::'+c);
                if(c.accountid==acc.id)
                {
                    if(acc.Contact_Names__c==null)
                    {
                        acc.Contact_Names__c=c.lastName;
                    }
                    else
                    {
                    acc.Contact_Names__c=acc.Contact_Names__c+';'+c.lastName;
                    }
                }
                system.debug('acc.Contact_Names__c::'+acc.Contact_Names__c);
            }
            aacL.add(acc);
            system.debug('aacL:::'+aacL);
        }
    }
    if(Trigger.isUpdate||Trigger.isDelete)
    {
        for(Account acc:accList)
        {
            acc.Contact_Names__c=null;
            for(Contact ct: acc.contacts)
            {
                if(acc.Contact_Names__c==null)
                {
                    acc.Contact_Names__c=ct.lastName;
                }
                else
                {
                    acc.Contact_Names__c=acc.Contact_Names__c+';'+ct.lastName;
                }
            }
            aacL.add(acc);     
        } 
    }
    if(!aacL.isempty())
    {
        update aacL;
    }
}