trigger ContactsCount on Contact (before insert, after insert) {
list<id> aid = new list<id> ();
Map <id,Account> accmap;
if(trigger.isinsert){
for(contact con: trigger.new){
aid.add(con.accountId);
}
list<Account> acc = [select id,Number_of_Contacts__c from Account where id in: aid];
list<Contact> cont = [select id from contact where accountid in: aid];
for(account act: acc){
act.Number_of_Contacts__c =cont.size();

}
update acc;
}
}

/* ****Another approch (Preferable)*****
trigger ContactsCount on Contact (before insert) {
list<id> aid = new list<id> ();
Map <id,Account> accmap;
List<account> accList= new List<account>();
if(trigger.isinsert){
for(contact con: trigger.new){
aid.add(con.accountId);
}

accmap=new Map <id,Account>([select id,Number_of_Contacts__c,(select id from contacts) from Account where id in: aid]);
for(contact act: trigger.new){
Account acc=accmap.get(act.accountId);
acc.Number_of_Contacts__c =acc.contacts.size()+1;
accList.add(acc);
}
update accList;
}
}*/