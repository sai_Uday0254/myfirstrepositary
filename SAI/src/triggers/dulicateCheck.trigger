trigger dulicateCheck on Account (before insert) {
   /* if there are more than 100 records which are inserting this will throw 
    * Too many soql :101 error  on the below code 
   */
    for(Account a:Trigger.New){
        Integer count=[select count() from Account where name=:a.name];
        if(count>0){
            a.addError('Duplicate Name Exists');
        }
    }
   

}