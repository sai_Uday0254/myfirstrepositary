trigger Master on Job_Application__c (before delete) {
set<id> jbid = new set<id> ();
for(Job_Application__c  jb: trigger.old){
jbid.add(jb.id);
}
//list<Job_Application__c > jblilst = new list<Job_Application__c >();
//jblilst = [select id, name,(select id from Candidate__r) from Job_Application__c  where Id In : jbid];

list<Candidate__c> cdlist=[select id,Job_Application__c from Candidate__c where Job_Application__c in: jbid ];

delete cdlist;

}