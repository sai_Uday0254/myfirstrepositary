trigger CandidateUpdate on Job_Application__c (after insert,after Update) {
list<Candidate__c> candlist = new list<Candidate__c>();
list<Job_Application__c> joblist = new list<Job_Application__c>();
set<Id> jobId = new set<Id>();
for(Job_Application__c ja: trigger.new){
jobId.add(ja.Id);
}
joblist = [select id,Education__c,(select id,Education__c from Candidate__r) from Job_Application__c where id=:jobId];
for(Job_Application__c jb:joblist ){
    if(jb.Candidate__r.size()==0){
        Candidate__c can = new Candidate__c();
can.Education__c = jb.Education__c;
can.Job_Application__c=jb.Id;
candlist.add(can);
}
    else
        for(Candidate__c candi:jb.Candidate__r){
        candi.Education__c = jb.Education__c;
        candlist.add(candi);
        }
}
upsert candlist;
}