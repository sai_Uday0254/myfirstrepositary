trigger GrandParentToGrandChild on Account (after update) {
map<id,list<OEF__c>> oefMap = new map<id,list<OEF__c>>();
set<id> accid = new set<id>();
list<oef__c> oefFinal = new list<oef__c>();
for(Account Ac: trigger.old){
accid .add(Ac.id);
}
Map<Id,Opportunity> oppmap = new Map<Id,Opportunity>([select id,name,AccountId,(select id,name from oef__r) from Opportunity where accountId in: accid]);
for(Opportunity opp: oppmap.values()){
if(oefMap.containskey(opp.AccountId))
{    
oefMap.get(opp.AccountId).addall(opp.OEF__r);
}
else
oefMap.put(opp.AccountId,opp.OEF__r);
}
system.debug('---oefMap----'+oefMap);
for(Account acc: Trigger.new){
list<oef__c> oeflist = oefMap.get(acc.Id);
for(oef__C oef: oeflist){
oef.Picklist_One__c = acc.Picklist_One__c;
oefFinal.add(oef);
system.debug('@@@result@@@'+oefFinal);
}
}
update oefFinal;
}