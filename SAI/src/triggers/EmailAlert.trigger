trigger EmailAlert on Contact (before insert) {
//Create a list to hold Emails we will send
list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
list<string> ConMails = new list<string>();
for(Contact con: Trigger.new){
if(con.Email !=null){
ConMails.add(con.Email);
system.debug('@@@@'+ConMails);
//Create a New email
Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage ();
mail1.setToAddresses(ConMails);
system.debug('####'+mail1);

//Sender details
mail1.setReplyTo('sai.bodhtree@gmail.com');
mail1.setSenderDisplayName('Sai BT');
//Subject and bodyof Email
mail1.setSubject('Email Alert Practiece');
string body = 'Dear' + con.Firstname;
mail1.setHtmlBody(body);
mails.add(mail1);
}
}
Messaging.sendEmail(mails);
}