trigger Jbnew on Job_Application__c (before insert) {
Set<id> ids= new Set<id>();

for(Job_Application__c jb :trigger.new)
{
   ids.add(jb.position__c);
}
Map<id,Position__c> mapP= new Map<id,Position__c>([select id,status__c from position__c where id in:ids]);
for(Job_Application__c jb :trigger.new)
{
 if(mapP.get(jb.position__c).status__c=='New Position'){
   jb.position__c.adderror('not able create');
   }
}
}