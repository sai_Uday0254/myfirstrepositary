trigger AmountUpdate on Job_Application__c (before insert, before update) {
set<id> Posid = new set<id>();
list<Position__c> pos2 = new list<Position__c>();
list<Position__c> pos = new list<Position__c>();
for(Job_Application__c  JA : trigger.new){
Posid.add(JA.Position__c);
}
pos =[select id,Amount__c from Position__c where id in :Posid ];
for(Position__c post:pos ){
for(Job_Application__c  JAP: trigger.new){
if(JAP.Position__c==post.id){
post.Amount__c =JAP.Amount__c ;
pos2.add(post);
}
}
}
update pos2;
}