trigger JbT on Job_Application__c (before insert) {
set<ID> jbids = new set<ID>();

for (Job_Application__c jba: trigger.new){
jbids.add(jba.position__c);
}
map<id,position__c> posmap = new map<id,position__c> ([select status__c from position__c where id In: jbids ]);
for(Job_Application__c jbap: trigger.new){
if(posmap.get(jbap.position__c).status__c =='New Position')
jbap.name.addError('My Test Two');
}
}