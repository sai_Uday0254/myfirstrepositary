trigger RecTypeUpdate on Opportunity (before insert,before update) {
Map<string,string> MapRecordType = new map<string,string>();
for(RecordType rt: [select name from RecordType where SObjectType =: 'OEF__c']){
MapRecordType.put(rt.name, rt.id);
}
if(trigger.isinsert){
for(Opportunity opp: trigger.new){
if(opp.Status__c!= null){
if(opp.Status__c=='One'){
opp.Rec_Type__c=MapRecordType.get('Rec One');
}
else if(opp.Status__c=='Two'){
opp.Rec_Type__c=MapRecordType.get('Rec Two');
}
else if(opp.Status__c=='Three'){
opp.Rec_Type__c=MapRecordType.get('Rec Three');
}
}
}
}
}