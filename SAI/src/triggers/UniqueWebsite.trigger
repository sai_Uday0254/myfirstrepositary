trigger UniqueWebsite on Account (before insert) {
//set<string> website = new set<string>();
map<string,string> websiteMap = new map<string,string>();

for(Account Acc: [select id,website from Account]){
//system.debug('@@@'+Acc.website);
websiteMap.put(Acc.website,Acc.Id);
}

for(Account ac: trigger.New){
system.debug('@@@'+websiteMap);
if(websiteMap.containsKey(ac.website)){
system.debug('@@@'+ac.website);
ac.AddError('Error Duplicate with Website');
}
}
}